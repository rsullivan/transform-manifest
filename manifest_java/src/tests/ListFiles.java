package tests;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import code.NatureFilenameFilter;

public class ListFiles {
	
	@Test
	public void getDigitsOnly(){
		System.out.println("another get digits only test");
		File dir = new File("xml");
		NatureFilenameFilter articlesOnly = new NatureFilenameFilter();
		String[] natureFiles = dir.list(articlesOnly);
		
		String regEx = "[^0-9]";
		//declare an ArrayList for later use
		ArrayList<String> digits_list = new ArrayList<String>();
		//declare an array to store results of split method
		String[] splitResults;
		
		//should make this nested loop a method that returns an array
		for(int i = 0; i<=natureFiles.length-1; i++){
			//create an array for each element in natureFiles
			splitResults = natureFiles[i].split(regEx);
			//iterate over all the arrays and add them to the ArrayList
			for(int j =0; j<=splitResults.length-1; j++){
				digits_list.add(j, splitResults[j]);
				
			}
			
			assertNotNull(digits_list);
		}
		
		
		
		//remove all empty elements from the ArraysList
		digits_list.removeAll(Arrays.asList(""));
		//make into an array to pass to stylesheet
		String[] digitsArray = new String[digits_list.size()];
		digitsArray = digits_list.toArray(digitsArray);
		
		for(String s: digitsArray){
			System.out.println(s);
		}
					
	}
	
	
}
