/**
 * 
 */
package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import code.ListArticles;

/**
 * @author robert
 *
 */
public class ManifestApp {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void listFilesForUser() {
		ListArticles listArticles = new ListArticles();
		String[]  expectedAnswer = {"nature11134.xml", "nature11153.xml", "nature11184.xml", "nature11185.xml"};
		String[] actualAnswer = listArticles.getArticles(); 
		assertEquals(expectedAnswer, actualAnswer);
	}

}
