package tests;

import static org.junit.Assert.*;

import java.io.File;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import net.sf.saxon.Configuration;
import net.sf.saxon.event.Receiver;
import net.sf.saxon.s9api.Destination;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;

import org.junit.Test;

import code.DestinationClass;

public class TransformerTests {
	
	
	/**
	 * javax.xml attempts
	 * 
	 */

	@Test
	public void test() {
		TransformerFactory factory = TransformerFactory.newInstance(); 
		assertNotNull("should not be null", factory);
	}
	
	@Test
	public void sameFactory(){
		
		TransformerFactory factory = TransformerFactory.newInstance();
		assertSame("should be same", factory, factory);
		
	}
	
	@Test
	public void getSourceXSLT(){
		
		Source xslt = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		assertNotNull("should not be null", xslt);
		
	}
	
	@Test
	public void setUpTransformer() throws TransformerConfigurationException{
		
		TransformerFactory factory = TransformerFactory.newInstance();
		Source xslt = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		Transformer transformer = factory.newTransformer(xslt);
		assertNotNull(transformer);
		
	}
	
	@Test
	public void transformManifest() throws TransformerException{
		
		Source xml = new StreamSource(new File("xml/pushlive-manifest.xml"));
		Source xslt = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(xslt);
		Result result = new StreamResult(new File("xml/output.xml"));
		transformer.transform(xml, result);
		assertNotNull(result);
		
	}
	
	
	/**
	 * below is an attempt with saxon
	 * 
	 */
	
	@Test
	public void setUpSaxon(){
		Configuration config = new Configuration();
		Processor processor = new Processor(config);
		assertNotNull(processor);
		
	}
	
	@Test
	public void createXSLTCompiler(){
		
		Configuration config = new Configuration();
		Processor processor = new Processor(config);
		XsltCompiler compiler = processor.newXsltCompiler();
		assertNotNull(compiler);
		
	}
	
	@Test
	public void compileStylesheet() throws SaxonApiException{
		
		Configuration config = new Configuration();
		Processor processor = new Processor(config);
		XsltCompiler compiler = processor.newXsltCompiler();
		Source stylesheet = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		XsltExecutable manifestTransform = compiler.compile(stylesheet);
		assertNotNull(manifestTransform);
	}
	
	@Test 
	public void createXsltTransformer() throws SaxonApiException{
		
		Configuration config = new Configuration();
		Processor processor = new Processor(config);
		XsltCompiler compiler = processor.newXsltCompiler();
		Source stylesheet = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		XsltExecutable manifestTransform = compiler.compile(stylesheet);
		XsltTransformer transformer = manifestTransform.load();//how to set options?
		assertNotNull(transformer);
		
		
		
	}
	
	@Test
	public void setTransformDestination()throws SaxonApiException{
		
		Configuration config = new Configuration();
		Processor processor = new Processor(config);
		XsltCompiler compiler = processor.newXsltCompiler();
		Source stylesheet = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		XsltExecutable manifestTransform = compiler.compile(stylesheet);
		XsltTransformer transformer = manifestTransform.load();//how to set options?
		Destination destination = new DestinationClass();
		Receiver output = destination.getReceiver(config);
		
		
		
	}
	

}
