package tests;

import static org.junit.Assert.*;

import java.io.File;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VariableTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	
		public void transformManifest() throws TransformerException{
			
			Source xml = new StreamSource(new File("xml/pushlive-manifest.xml"));
			Source xslt = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
			TransformerFactory factory = TransformerFactory.newInstance();
			Transformer transformer = factory.newTransformer(xslt);
			Result result = new StreamResult(new File("xml/output.xml"));
			transformer.transform(xml, result);
			assertNotNull(result);
			
		}
	
	@Test
	public void setParametersTest() throws TransformerException{
		
		
		Source xml = new StreamSource(new File("xml/pushlive-manifest.xml"));
		Source xslt = new StreamSource(new File("xml/manifest_stylesheet.xsl"));
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer(xslt);
		Result result = new StreamResult(new File("xml/output.xml"));
		String[] articleDigits = {"11190", "11184"};
		transformer.setParameter("articles", articleDigits);
		transformer.transform(xml, result);
		assertNotNull(result);
		
		
	}
	}


