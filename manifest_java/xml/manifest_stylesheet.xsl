<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="push-list">
        <pushlive-filespec>
            <web-root>NPG</web-root>
            <push-list>
                <xsl:apply-templates select="file"/>
            </push-list>
        </pushlive-filespec>
    </xsl:template>


    <xsl:template match="web-root"/>

    <!-- ONLY EDIT THIS NEXT LINE list the desired article numbers here -->
    <xsl:param name="articles" select="('')"/>


    <!-- DO NOT EDIT THE REST OF THIS FILE -->
    <xsl:template match="/pushlive-filespec/push-list/file" name="matches">
        <xsl:variable name="doi" select="."/>


        <!-- generic file reg ex 
                <file>/nature/journal/vaop/ncurrent/index.html</file>
                <file>/nature/journal/vaop/ncurrent/rights.xml</file>
                <file>/nature/journal/vaop/ncurrent/rss-fb.rdf</file>
                <file>/nature/journal/vaop/ncurrent/rss.rdf</file>
                <file>/nature/journal/vaop/ncurrent/xprofiles/master_0.prf</file>
                <file>/nature/journal/vaop/ncurrent/xprofiles/master_articles_0.prf</file>
                ([\D0]+) - this matches anything that does not contain any digit from 1 to 9. 
            -->

        <!-- Get any files which do not contain any digits from 1 to 9 -->
        <xsl:if test="not(matches($doi,'[1-9]'))">
            <file>
                <xsl:value-of select="."/>
            </file>
        </xsl:if>

<!-- extract the numeric id from the article filename into variable $this-doi-id, e.g. "11854" from "nature11854-f1.2.jpg" -->
        <xsl:variable name="this-doi-id">
            <xsl:analyze-string select="$doi" regex="/[a-z_-]{{6,}}.*(\d{{5}}).*">
                <xsl:matching-substring>
                    <xsl:value-of select="regex-group(1)"/>
                </xsl:matching-substring>
            </xsl:analyze-string>
        </xsl:variable>

<!-- run through list of given numeric ids in sequence variable $articles -->
<!-- if any matches exactly the numeric id from the current <file> content, output that <file> -->
        <xsl:for-each select="$articles">
            <xsl:variable name="this-article" select="."/>
            <xsl:if test="$this-doi-id=$this-article">
                <file>
                    <xsl:value-of select="$doi"/>
                </file>
            </xsl:if>
        </xsl:for-each>

    </xsl:template>

</xsl:stylesheet>
